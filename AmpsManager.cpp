//AmpsManager.cpp

////@begin includes
////@end includes
#include <fstream>
#include <iomanip>
//#include "mainamps.h"
//#include "ambient.h"
//#include "materialdlg.h"
//#include "resdisplay.h"

#include "Calculation.h"
#include "NonEqui.h"
#include "Constant.h"
#include "Point.h"
#include "Point2.h"
#include <math.h>
#include <algorithm>
#include "LU.h"
//#include "main.rc"
using namespace std;

#include "AmpsManager.h"
#include "utilities.h"

//WX_DEFINE_LIST(DefectList);
//WX_DEFINE_LIST(MyList);

layer::layer()
{
	Epson=_("0");Eg=_("0");affi=_("0");Nc=_("0");Nv=_("0");un=_("0");up=_("0");n=_("0");p=_("0");
	name=_(" ");thickness=_("0");
	absA=_("0");absB=_("0");
	BgShape=0;IsBT=0;EgOpt=_("0");
	CBTEnergy=_("0.01");CBTGo=_("1e14");CBTSigN=_("1e-17");CBTSigP=_("1e-15");
	VBTEnergy=_("0.01");VBTGo=_("1e14");VBTSigN=_("1e-15");VBTSigP=_("1e-17");
	MGEnergy=_("0.56");GMGA=_("1e12");SigNMGA=_("1e-17");SigPMGA=_("1e-15");
	GMGD=_("1e12");SigNMGD=_("1e-15");SigPMGD=_("1e-17");
	hEdge=_("0.5");hCenter=_("20");

}

layer::layer(layer& matPar)//deep copy
{
	absA=matPar.absA; absB=matPar.absB;absWave=matPar.absWave;alpha=matPar.alpha;
	affi=matPar.affi;Eg=matPar.Eg;Epson=matPar.Epson;n=matPar.n;p=matPar.p;Nc=matPar.Nc;Nv=matPar.Nv;un=matPar.un;up=matPar.up;
	name=matPar.name;thickness=matPar.thickness;

	IsBT=matPar.IsBT; EgOpt=matPar.EgOpt;
	CBTEnergy=matPar.CBTEnergy;CBTGo=matPar.CBTGo;CBTSigN=matPar.CBTSigN;CBTSigP=matPar.CBTSigP;
	VBTEnergy=matPar.VBTEnergy;VBTGo=matPar.VBTGo;VBTSigN=matPar.VBTSigN;VBTSigP=matPar.VBTSigP;
	BgShape=matPar.BgShape;
	MGEnergy=matPar.MGEnergy;GMGA=matPar.GMGA;SigNMGA=matPar.SigNMGA;SigPMGA=matPar.SigPMGA;
	GMGD=matPar.GMGD;SigNMGD=matPar.SigNMGD;SigPMGD=matPar.SigPMGD;

	int defCnt=matPar.defList.size();

	for(int i=0;i<defCnt;i++)
	{
		defect *curDef=matPar.defList[i];
		defect *newDef=new defect;
		*newDef=*curDef;
		defList.push_back(newDef);
	}
}
layer::~layer()
{
	ClearVector<defect>(defList);
}
void layer::copy(layer& matPar)//deep copy
{
	absA=matPar.absA; absB=matPar.absB;absWave=matPar.absWave;alpha=matPar.alpha;
	affi=matPar.affi;Eg=matPar.Eg;Epson=matPar.Epson;n=matPar.n;p=matPar.p;Nc=matPar.Nc;Nv=matPar.Nv;un=matPar.un;up=matPar.up;
	name=matPar.name;thickness=matPar.thickness;

	BgShape=matPar.BgShape;IsBT=matPar.IsBT;EgOpt=matPar.EgOpt;
	CBTEnergy=matPar.CBTEnergy; CBTGo=matPar.CBTGo;CBTSigN=matPar.CBTSigN;CBTSigP=matPar.CBTSigP;
	VBTEnergy=matPar.VBTEnergy; VBTGo=matPar.VBTGo;VBTSigN=matPar.VBTSigN;VBTSigP=matPar.VBTSigP;
	MGEnergy=matPar.MGEnergy;
	GMGA=matPar.GMGA;SigNMGA=matPar.SigNMGA;SigPMGA=matPar.SigPMGA;
	GMGD=matPar.GMGD;SigNMGD=matPar.SigNMGD;SigPMGD=matPar.SigPMGD;

	hEdge=matPar.hEdge;hCenter=matPar.hCenter;

	int defCnt=matPar.defList.size();

	ClearVector<defect>(defList);
	
	for(int i=0;i<defCnt;i++)
	{
		defect *curDef=matPar.defList[i];
		defect *newDef=new defect;
		*newDef=*curDef;
		defList.push_back(newDef);
	}
}

/*
Need to load in a device, take its eqe and print it out.
*/

AmpsManager::AmpsManager() : pt(NULL)
{
	settings.model = 0; //Tunneling model
	settings.eps = 1e-6; //Required convergence precision
	settings.clamp = 0.05; 
	settings.iterTimes = 500; //Related to the maximum number of iterations
	settings.F0 = 1e5; //Trap Assisted Tunneling (TAT) parameter
	
	//Initialize default ambient settings
	ambient.temperature = 300;
	ambient.light = false;
	ambient.useWF = false;
	ambient.doQE = true;
	ambient.topSn = 1e7;
	ambient.topSp = 1e7;
	ambient.bottomSn = 1e7;
	ambient.bottomSp = 1e7;
	ambient.topReflectance = 0.0;
	ambient.bottomReflectance = 0.0;
	
	iqeCutoff = 0.01;
}

AmpsManager::~AmpsManager()
{
	if (pt)
		delete pt;
	
	//FIXME: Delete layers if there are any
	ClearVector<layer>(LayerList);
}

void AmpsManager::loadDevice(const std::string &path)
{
	wxString wxPath(path.c_str());
	long lyrCnt=LayerList.size();//delete old data
	
	ClearVector<layer>(LayerList);
		
	TiXmlDocument *myDocument = new TiXmlDocument(wxPath.To8BitData());
	myDocument->LoadFile();
	if(myDocument->Error())
	{
		//printf(myDocument->ErrorDesc());
		exit(1);
	}
	//	try
	{
		TiXmlElement *RootElement = myDocument->RootElement();
		wxString cnt=wxString::FromAscii(RootElement->FirstAttribute()->Value());
		TiXmlElement *LyrElement=RootElement->FirstChildElement();
		cnt.ToLong(&lyrCnt);
		for(int i=0;i<lyrCnt;i++)//load and add layer
		{
			layer *curLyr=new layer;
			loadLayer(LyrElement,curLyr);
			LayerList.push_back(curLyr);
			LyrElement=LyrElement->NextSiblingElement();
		}
	}
	
	delete myDocument;
}

void AmpsManager::loadLayer(TiXmlElement *LyrElement, layer* curLyr)
{
	curLyr->name=wxString::FromAscii(LyrElement->FirstAttribute()->Value());
	curLyr->thickness=wxString::FromAscii(LyrElement->FirstAttribute()->Next()->Value());
	wxString temp=wxString::FromAscii(LyrElement->Value());

	if(temp!=_("Material"))
	{
		printf(_("file format error!"));
		return;
	}
	TiXmlElement* ElecElement=LyrElement->FirstChildElement();
	TiXmlElement* DielecElement=ElecElement->FirstChildElement(); curLyr->Epson=wxString::FromAscii(DielecElement->FirstChild()->Value());
	TiXmlElement* EgElement=DielecElement->NextSiblingElement(); curLyr->Eg=wxString::FromAscii(EgElement->FirstChild()->Value());
	TiXmlElement *AffiElement=EgElement->NextSiblingElement(); curLyr->affi=wxString::FromAscii(AffiElement->FirstChild()->Value());
	TiXmlElement* NcElement=AffiElement->NextSiblingElement(); curLyr->Nc=wxString::FromAscii(NcElement->FirstChild()->Value());
	TiXmlElement* NvElement=NcElement->NextSiblingElement(); curLyr->Nv=wxString::FromAscii(NvElement->FirstChild()->Value());
	TiXmlElement* UnElement=NvElement->NextSiblingElement(); curLyr->un=wxString::FromAscii(UnElement->FirstChild()->Value());
	TiXmlElement* UpElement=UnElement->NextSiblingElement(); curLyr->up=wxString::FromAscii(UpElement->FirstChild()->Value());
	TiXmlElement* NElement=UpElement->NextSiblingElement(); curLyr->n=wxString::FromAscii(NElement->FirstChild()->Value());
	TiXmlElement* PElement=NElement->NextSiblingElement(); curLyr->p=wxString::FromAscii(PElement->FirstChild()->Value());
	/*---------Defect------------*/
	TiXmlElement *DefElement = ElecElement->NextSiblingElement();
	temp=wxString::FromAscii(DefElement->FirstAttribute()->Value());
	long defCnt; temp.ToLong(&defCnt);
	TiXmlElement *DElement=DefElement->FirstChildElement();
	ClearVector<defect>(curLyr->defList);

	for(int i=0;i<defCnt;i++)//ergodic
	{
		defect * curDef=new defect;		curDef->name=wxString::FromAscii(DElement->Value());
		TiXmlElement* Value=DElement->FirstChildElement(); long v;
		temp=wxString::FromAscii(Value->FirstChild()->Value());temp.ToLong(&v);curDef->type=(int)v;
		Value=Value->NextSiblingElement();temp=wxString::FromAscii(Value->FirstChild()->Value());temp.ToLong(&v);curDef->dist=(int)v;
		Value=Value->NextSiblingElement();curDef->den=wxString::FromAscii(Value->FirstChild()->Value());
		Value=Value->NextSiblingElement();curDef->el=wxString::FromAscii(Value->FirstChild()->Value());
		Value=Value->NextSiblingElement();curDef->width=wxString::FromAscii(Value->FirstChild()->Value());
		Value=Value->NextSiblingElement();curDef->sigN=wxString::FromAscii(Value->FirstChild()->Value());
		Value=Value->NextSiblingElement();curDef->sigP=wxString::FromAscii(Value->FirstChild()->Value());		
		curLyr->defList.push_back(curDef);
		DElement=DElement->NextSiblingElement();
	}
	temp=wxString::FromAscii(DElement->FirstAttribute()->Value());//BTElement
	long IsBT;temp.ToLong(&IsBT);
	if(IsBT)
	{
		curLyr->IsBT=1;
		TiXmlElement* CBTElement=DElement->FirstChildElement();
		TiXmlElement* Value=CBTElement->FirstChildElement();curLyr->CBTEnergy=wxString::FromAscii(Value->FirstChild()->Value());
		Value=Value->NextSiblingElement();curLyr->CBTGo=wxString::FromAscii(Value->FirstChild()->Value());
		Value=Value->NextSiblingElement();curLyr->CBTSigN=wxString::FromAscii(Value->FirstChild()->Value());
		Value=Value->NextSiblingElement();curLyr->CBTSigP=wxString::FromAscii(Value->FirstChild()->Value());
		TiXmlElement* VBTElement=CBTElement->NextSiblingElement();
		Value=VBTElement->FirstChildElement();curLyr->VBTEnergy=wxString::FromAscii(Value->FirstChild()->Value());
		Value=Value->NextSiblingElement();curLyr->VBTGo=wxString::FromAscii(Value->FirstChild()->Value());
		Value=Value->NextSiblingElement();curLyr->VBTSigN=wxString::FromAscii(Value->FirstChild()->Value());
		Value=Value->NextSiblingElement();curLyr->VBTSigP=wxString::FromAscii(Value->FirstChild()->Value());

		TiXmlElement* MGElement=VBTElement->NextSiblingElement();
		temp=wxString::FromAscii(MGElement->FirstAttribute()->Value());
		long BgShape;temp.ToLong(&BgShape);
		if(BgShape)
		{
			curLyr->BgShape=1;
			Value=MGElement->FirstChildElement();curLyr->MGEnergy=wxString::FromAscii(Value->FirstChild()->Value());
			Value=Value->NextSiblingElement();curLyr->GMGA=wxString::FromAscii(Value->FirstChild()->Value());
			Value=Value->NextSiblingElement();curLyr->SigNMGA=wxString::FromAscii(Value->FirstChild()->Value());
			Value=Value->NextSiblingElement();curLyr->SigPMGA=wxString::FromAscii(Value->FirstChild()->Value());
			Value=Value->NextSiblingElement();curLyr->GMGD=wxString::FromAscii(Value->FirstChild()->Value());
			Value=Value->NextSiblingElement();curLyr->SigNMGD=wxString::FromAscii(Value->FirstChild()->Value());
			Value=Value->NextSiblingElement();curLyr->SigPMGD=wxString::FromAscii(Value->FirstChild()->Value());
		}
	}
	//----------Optical-----------------
	double wave, abs;wxString str;
	TiXmlElement* OptElement=DefElement->NextSiblingElement();
	TiXmlElement * AElement=OptElement->FirstChildElement();
	curLyr->absA=wxString::FromAscii(AElement->FirstChild()->Value());
	TiXmlElement * BElement=AElement->NextSiblingElement();
	curLyr->absB=wxString::FromAscii(BElement->FirstChild()->Value());
	TiXmlElement * AbsElement=BElement->NextSiblingElement();
	curLyr->absWave.clear();curLyr->alpha.clear();
	for(TiXmlElement* waveElement=AbsElement->FirstChildElement();waveElement;waveElement=waveElement->NextSiblingElement())//ergodic
	{
		str=wxString::FromAscii(waveElement->FirstAttribute()->Value());
		str.ToDouble(&wave);
		curLyr->absWave.push_back(wave*1e-9);
		str=wxString::FromAscii(waveElement->FirstChild()->Value());
		str.ToDouble(&abs);
		curLyr->alpha.push_back(abs);
	}
	//-----------advance--------------
	TiXmlElement* h=OptElement->NextSiblingElement();
	curLyr->hEdge=wxString::FromAscii(h->FirstChild()->Value());
	TiXmlElement* hc=h->NextSiblingElement();
	curLyr->hCenter=wxString::FromAscii(hc->FirstChild()->Value());
}

void AmpsManager::eqe(const std::string &path)
{	
	ambient.temperature = 300;
	ambient.light = false;
	ambient.useWF = false;
	ambient.doQE = true;
	ambient.topSn = 1e7;
	ambient.topSp = 1e7;
	ambient.bottomSn = 1e7;
	ambient.bottomSp = 1e7;
	ambient.topReflectance = 0.0;
	ambient.bottomReflectance = 0.0;
	
	this->loadDevice(path);
	this->solve();
}

int AmpsManager::calculateThickness()
{
	double hEdge, hCenter;
	int lyrCnt=LayerList.size();
	double L,sum=0,thickness=0;
	//int *grids=new int[lyrCnt];double *q=new double[lyrCnt];
	vector<int>grids; vector<double> q;
	for(int num=0;num<lyrCnt;num++)
	{
		//layer * cur=LayerList.GetAt(m_layerList.FindIndex(num));
		layer *curLyr=LayerList[num];
		curLyr->hEdge.ToDouble(&hEdge);curLyr->hCenter.ToDouble(&hCenter);
		curLyr->thickness.ToDouble(&L);//unit um
		if(hEdge<hCenter)
		{
		q.push_back((L-2*hEdge*1e-3)/(L-2*hCenter*1e-3));
		grids.push_back(2*(int)(log(hCenter/hEdge)/log(q[num])+1));
		}
		else
		{
			q.push_back(1);
			grids.push_back(int(L*1e3/hCenter));
		}
		/*q[num]=(L-2*hEdge*1e-3)/(L-2*hCenter*1e-3);
		grids[num]=2*(int)(log(hCenter/hEdge)/log(q[num])+1);*/
		sum+=grids[num]+1;
		thickness+=L;
	}
	
	return thickness;
}

void AmpsManager::reverse()
{
	std::reverse(LayerList.begin(), LayerList.end());
}

void AmpsManager::solve()
{
	int prgCnt = 0;
	double vOld;
	
	ResultArray ResArray;
	resultPar	resPar;
	
	if(LayerList.empty())
	{
		//printf(_("Input material information"));
		printf("Empty Layer List");
		return;
	}
	if(/*wxYES==printf(_("start calculation?"),_("confirm"),wxYES_NO)*/ true)
	{
		//Copy all parameters to local variables from ambient
		double temp,TopSn,TopSp,TopRF,BtmSn,BtmSp,BtmRF,TopWF,BtmWF;
		
		temp = ambient.temperature;
		TopSn = ambient.topSn;
		TopSp = ambient.topSp;
		BtmSn = ambient.bottomSn;
		BtmSp = ambient.bottomSp;
		TopWF = ambient.topWF;
		BtmWF = ambient.bottomWF;
		TopRF = ambient.topReflectance;
		BtmRF = ambient.bottomReflectance;

		double kT=temp*KBEV;double thickness;int PointNumber;
		vector<double>I,V;		result *curRes;
		//show process dialog
		prgCnt=0; wxString str,title;
		ClearVector<result>(ResArray);	resPar.V.clear();resPar.I.clear();
		
		//if not initialized
		//if(!IsInit)
		{
			PointNumber = 0.;
			thickness = this->calculateThickness();
			if(0==thickness)
			{
				//printf(_("thickness is 0!"));
				//dlg->Destroy();
				printf("Thickness is 0!");
				return;
			}
			else//(PointNumber>2)
			{
				if(pt!=NULL)
				{
					delete pt;pt=NULL;
				}
				{
					//pt=new Point(LayerList,kT);
					pt=new Point2(LayerList,kT);

					PointNumber=pt->PointNumber;
					pt->precision = settings.eps;
					pt->limit = settings.clamp;
					pt->F0 = settings.F0;
					pt->iterTimes = settings.iterTimes;
				}
				
				{	//optimize the initial value
					pt->psiInit[0]=pt->affi[0];//-pt->affi[PointNumber-1];//-kT*log(pt->Nd[0]*pt->Nc[PointNumber-1]/pt->Nd[PointNumber-1]/pt->Nc[0]);
					if(pt->Nd[0]!=0)
						pt->psiInit[0]+=kT*log(pt->Nc[0]/pt->Nd[0]);
					if(pt->Na[0]!=0)
						pt->psiInit[0]+=pt->Eg[0]+kT*log(pt->Na[0]/pt->Nv[0]);//increase the barrier
					pt->psiInit[PointNumber-1]=pt->affi[PointNumber-1];
					if(pt->Nd[PointNumber-1]!=0)
						pt->psiInit[PointNumber-1]+=kT*log(pt->Nc[PointNumber-1]/pt->Nd[PointNumber-1]);
					if(pt->Na[PointNumber-1]!=0)
						pt->psiInit[PointNumber-1]+=pt->Eg[PointNumber-1]+kT*log(pt->Na[PointNumber-1]/pt->Nv[PointNumber-1]);
					//pt->Efn[i]=0;pt->Efp[i]=0;
					for(int i=1;i<PointNumber-1;i++)
					{
						pt->psiInit[i]=pt->psiInit[0]+(pt->psiInit[PointNumber-1]-pt->psiInit[0])*(pt->x[i]/thickness);
					}
				}
				//-----------Initialization-------------			
				curRes=pt->ThermalInit(ambient.useWF,TopWF,BtmWF,NULL,prgCnt);
				vOld=0;
			
				//--------------------------------------
				if(curRes!=NULL)
				{
					ResArray.push_back(curRes);resPar.V.push_back(0);resPar.I.push_back(0);
				}
				else
				{					
					//dlg->Destroy();
					return;
				}				
			}			
		}
		double current;
		resPar.Effi=0;resPar.FF=0;resPar.Jsc=0;resPar.Voc=0;
		resPar.max=0;
		if(!ambient.light&&!ambient.voltages.empty())//&&voltage!=0)//dark
		{
			int drtn=1;//test PN direction
			if((pt->n0L>pt->p0L)&&(pt->n0R<pt->p0R))drtn=-1;

			int vCnt=ambient.voltages.size();
			for(int i=0;i<pt->PointNumber;i++)//clear G
				pt->G[i]=0;
			//title=_("Function:Dark Condition\n");str=_("Initializing ");str=title+str;
			//dlg->Update(++prgCnt%100,str);
			ClearVector<result>(ResArray);resPar.V.clear();resPar.I.clear();
			Point ptGummel(LayerList,kT); 
			result* GummelRes;
			ptGummel.precision = settings.eps;
			ptGummel.limit = settings.clamp;
			ptGummel.iterTimes = settings.iterTimes;
			
			vOld=0;
			for(int j=0;j<vCnt;j++)
			{
				//str.Printf(_("V=%fv"),ambient.voltages[j]);str=title+str;
				/*curRes=NonEquiTun(drtn*ambient.voltages[j],pt,PointNumber,kT,
					ambient.IsWF,TopWF,TopSn,TopSp,BtmWF,BtmSn,BtmSp,
					dlg,str,prgCnt);*/
				curRes=pt->NonEquiNew(vOld,drtn*ambient.voltages[j],TopWF,TopSn,TopSp,BtmWF,BtmSn,BtmSp,
					NULL,str,prgCnt);
				vOld=drtn*ambient.voltages[j];
				if(0==settings.model)
				{
					if(curRes!=NULL)
					{
						ResArray.push_back(curRes);
						current=pt->Jn[0]+pt->Jp[0];
					}
					else
						break;

				}
				else if(1==settings.model)
				{					
					if(curRes!=NULL)
					{
						delete curRes;					
						pt->ToPoint(ptGummel);
						GummelRes=NonEquiTun(drtn*ambient.voltages[j],&ptGummel,ptGummel.PointNumber,kT,
							ambient.useWF,TopWF,TopSn,TopSp,BtmWF,BtmSn,BtmSp,
							NULL,str,prgCnt);
						if(GummelRes!=NULL)
						{
							ResArray.push_back(GummelRes);
							current=ptGummel.Jn[0]+ptGummel.Jp[0];
							//pt->FromPoint(ptGummel);
						}
						else
						{
							//str.Printf(_("Gummel's method failed in voltage=%fv"),ambient.voltages[j]);
							//printf(str);
							//dlg->Destroy();
							return;//break;
						}
					}
					else 
						break;
				}

				I.push_back(current*1e3);V.push_back(ambient.voltages[j]);
				//str.Printf(_("V=%fv, J=%.4emA/cm2"),ambient.voltages[j],I[j]);str=title+str;
			}
			resPar.I=I;
			resPar.V=V;
		}
		int size=ambient.voltages.size();
		if(ambient.light)//light
		{
			Point ptGummel(LayerList,kT);	
			ptGummel.precision = settings.eps;
			ptGummel.limit = settings.clamp;
			ptGummel.iterTimes = settings.iterTimes;
			
			if(!ExportG(&ptGummel,TopRF,BtmRF,ambient.photons,ambient.wavelengths))
			{
				//dlg->Destroy();
				return;
			}			
			pt->ExportG(TopRF,BtmRF,ambient.photons,ambient.wavelengths);
			double Jsc;
			//title=_("Function:Light Condition\n");
			//str=_("Calculating Jsc ");str=title+str;dlg->Update(++prgCnt%100,str);
			ClearVector<result>(ResArray);resPar.V.clear();resPar.I.clear();
			//calculate Jsc and initialize			
			{
				pt->ToPoint(ptGummel);
				curRes=NonEquiInit(0,&ptGummel,ptGummel.PointNumber,kT,ambient.useWF,TopWF,TopSn,TopSp,BtmWF,BtmSn,BtmSp,NULL,str,prgCnt);
				delete curRes;
				pt->FromPoint(ptGummel);
				vOld=0; 
				curRes=pt->NonEquiNew(0,0,TopWF,TopSn,TopSp,BtmWF,BtmSn,BtmSp,NULL,str,prgCnt);
				if(curRes!=NULL)
					delete curRes;
				else
				{
					//dlg->Destroy();printf(_("Failed in initialization!"));
					return;
				}
			}			
			Jsc=pt->Jn[0]+pt->Jp[0];
			//Jsc=ptGummel.Jn[0]+ptGummel.Jp[0];	
			
			int drtn0;
			if(Jsc>0)drtn0=1;else drtn0=-1;
			int drtn1=drtn0;

			//str.Printf(_("Jsc=%fmA/cm2"),drtn0*Jsc*1e3);str=title+str;
			//dlg->Update(++prgCnt%100,str);
	
			int i=0;			
			do
			{
				curRes=pt->NonEquiNew(vOld,-ambient.voltages[i]*drtn0,TopWF,TopSn,TopSp,BtmWF,BtmSn,BtmSp,
					NULL,str,prgCnt);
				vOld=-ambient.voltages[i]*drtn0;
				if(0==settings.model)
				{
					if(curRes!=NULL)
					{					
						ResArray.push_back(curRes);
						current=pt->Jn[0]+pt->Jp[0];							
					}
					else 
						break;
				}
				else if(1==settings.model)
				{
					pt->ToPoint(ptGummel);			result * GummelRes;	
					GummelRes=NonEquiTun(-ambient.voltages[i]*drtn0,&ptGummel,ptGummel.PointNumber,kT,
						ambient.useWF,TopWF,TopSn,TopSp,BtmWF,BtmSn,BtmSp,
						NULL,str,prgCnt);
					if(GummelRes!=NULL)
					{
						delete curRes;ResArray.push_back(GummelRes);
						current=ptGummel.Jn[0]+ptGummel.Jp[0];			
						//pt->FromPoint(ptGummel);
					}
					else
					{
						//str.Printf(_("Gummel's method failed in voltage=%fv"),ambient.voltages[i]);
						ResArray.push_back(curRes);
						current=pt->Jn[0]+pt->Jp[0];	
						//printf(str);
						break;
					}
				}
				
				drtn1=int(current/fabs(current));//? current==0
				if(vOld==0)Jsc=current;
				I.push_back(drtn0*current*1e3);V.push_back(ambient.voltages[i]);
				//str.Printf(_("V=%fv, J=%fmA/cm2"),ambient.voltages[i],I[i]);str=title+str;
				
				i++;
			}while((drtn1==drtn0)&&i<size);			

			int n=I.size();
			if(n>=2)
				resPar.Voc=V[n-2]+log(1+(exp(V[n-1]-V[n-2])-1)*I[n-2]/(I[n-2]-I[n-1]));

			double pMax=0;
			for( i=0;i<n;i++)
			{
				if(I[i]*V[i]>pMax)
				{
					pMax=I[i]*V[i];
					resPar.max=i;
				}
			}
			Jsc=drtn0*Jsc;//turn Jsc to be positive
			resPar.Jsc=Jsc*1e3;
			resPar.FF=pMax/resPar.Jsc/resPar.Voc*100;
			resPar.Effi=pMax;//%
			resPar.I=I;
			resPar.V=V;
			
			this->currents = I;
		}
		if(ambient.doQE)
		{
			double beam=1e15, esc = 0.0;
			
			resPar.QEffi.clear();
			//title=_("Function: Quantum Efficiency\n");
			Point ptGummel(LayerList,kT);		
			ptGummel.precision = settings.eps;
			ptGummel.limit = settings.clamp;
			ptGummel.iterTimes = settings.iterTimes;
			
			bool reCalc=0;int cnt=0;
			
			quantumEfficiencies.clear();
			internalQE.clear();
			
			for(size_t i=0;i<wavelengths.size();i++)
			{
				vector<double> phi, waveLength;
				std::vector<double> escaped;
				
				phi.push_back(beam);
				waveLength.push_back(wavelengths[i]*1e-9);								
											
				pt->ExportG(TopRF,BtmRF,phi,waveLength, &escaped);
				esc = escaped[0];

				if(0==i||reCalc)//Gummel initialize
				{
					if(!ExportG(&ptGummel,TopRF,BtmRF,phi,waveLength))
					{
						//dlg->Destroy();
						return;
					}	
					for(int j=0;j<pt->PointNumber;j++)//initialize the pt
					{
						pt->psi[j]=pt->psiInit[j];pt->Efn[j]=0;pt->Efp[j]=0;
					}
					pt->ToPoint(ptGummel);
					curRes=NonEquiInit(0,&ptGummel,ptGummel.PointNumber,kT,ambient.useWF,TopWF,TopSn,TopSp,BtmWF,BtmSn,BtmSp,NULL,str,prgCnt);
					delete curRes;
					pt->FromPoint(ptGummel);
					reCalc=0;
				}				
				curRes=pt->NonEquiNew(0,0,TopWF,TopSn,TopSp,BtmWF,BtmSn,BtmSp,
					NULL,str,prgCnt);
				if(curRes!=NULL)
				{
					delete curRes;cnt=0;
					double Jsc;
					if(0==settings.model)
						Jsc=pt->Jn[0]+pt->Jp[0];
					else if(1==settings.model)
					{
						pt->ToPoint(ptGummel); result * GummelRes;
						ExportG(&ptGummel,TopRF,BtmRF,phi,waveLength);
						GummelRes=NonEquiTun(0,&ptGummel,ptGummel.PointNumber,kT,
							ambient.useWF,TopWF,TopSn,TopSp,BtmWF,BtmSn,BtmSp,
							NULL,str,prgCnt);
						if(GummelRes!=NULL)
						{
							ResArray.push_back(GummelRes);
							Jsc=ptGummel.Jn[0]+ptGummel.Jp[0];			
							//pt->FromPoint(ptGummel);
						}
						else
						{
							//str.Printf(_("Gummel's method failed in waveLength=%fnm"),waveLength[i]*1e9);
							//printf(str);							
							//break;
						}
						//Jsc=ptGummel.Jn[0]+ptGummel.Jp[0];
					}				
					resPar.QEffi.push_back(fabs(Jsc)/QE/beam);
					
					
					if(resPar.QEffi.back()>1.01) //FIXME: This should not be necessary
					{
						//If we're too high, recalculate
						resPar.QEffi.pop_back();i--;
						reCalc=1;
					}
					else if(resPar.QEffi.back()>1.0)
					{
						//If the error's not that big, clamp to 1.0
						resPar.QEffi.pop_back();
						resPar.QEffi.push_back(1.0);
					}
					
					if (reCalc == 0)
					{
						//FIXME: add check for clamping at unity
						esc = escaped[0];
						
						double photons = fabs(Jsc)/QE;
						double abs = beam - esc;
						
						//printf("Escaped: %g\nPhotons: %g\n", esc, photons);
						
						quantumEfficiencies.push_back(resPar.QEffi.back());
						
						//If we absorb less than a certain cutoff of the light, set the iqe is 0 to avoid blowing up
						if (abs > (beam*iqeCutoff))
						{
							double iqe = photons/abs;
							
							//Clamp IQE to 1.0, the error shouldn't be big since we already checked the EQE above
							if (iqe > 1.0)
								iqe = 1.0;

							internalQE.push_back(iqe);
						}
						else
							internalQE.push_back(0.0);
					}
				}
				else
				{
					reCalc=1; 	
					if((++cnt)>1){
						//str.Printf(_("failed in waveLength=%.0fnm, skip to next wave length"),waveLength[0]*1e9); 
						//printf(str);			
						double value=(double)resPar.QEffi.back();
						resPar.QEffi.push_back(value);
						continue;
					}
					i--;					
				}
				
				//printf("wavelengths %d: %.2f\n", (300+20*i), resPar.QEffi.back());
			}
		}
	}
	
	//Fixed memory leak: Clear the ResArray
	ClearVector<result>(ResArray);
}

std::vector<double> AmpsManager::getEQEWavelengths()
{
	return this->wavelengths;
}

std::vector<double> AmpsManager::getEQEResults()
{
	return this->quantumEfficiencies;
}

std::vector<double> AmpsManager::getIQEResults()
{
	return this->internalQE;
}

bool AmpsManager::ExportG(Point *pt,double TopRF,double BtmRF,vector<double> photo0,vector<double> waveLength, vector<double> *escaped)
{
	int region=0,rgn=LayerList.size();
	int size=waveLength.size();double absi,geni;
	double genSum, cumAbs=0.0;
	double x0=0;
	//vector<double> photo0=ambPar.photonNum;
	for(int i=0;i<size;i++)
		photo0[i]*=(1-TopRF);

	layer *curLyr=LayerList[region];
	for(int i=0;i<rgn;i++)
	{
		layer* testLyr=LayerList[i];
		if(!testLyr->absWave.size())
		{
//			wxString str=_("No absorption coefficient in material ");
//			str+=testLyr->name;
//			printf(str);
			//FIXME: Add error checking and setting of appropriate flags
			return 0;
		}
	}
	for(int i=0;i<pt->PointNumber;i++)//x[i]
	{
		{
			genSum=0;
			for(int j=0;j<size;j++)//waveLength[j]
			{
				//double wL=waveLength[j];
				absi=1e-2*Interp(curLyr->absWave,curLyr->alpha,waveLength[j]);
				geni=absi*photo0[j]*exp(-absi*(pt->x[i]-x0));
				genSum+=geni;
			}
			pt->G[i]=genSum;
			cumAbs += genSum;
		}
		if(pt->step[i]==0)// into another region
		{			
			int waveNum=photo0.size();
			for(int k=0;k<waveNum;k++)
			{
				absi=1e-2*Interp(curLyr->absWave,curLyr->alpha,waveLength[k]);
				photo0[k]=photo0[k]*exp(-absi*(pt->x[i]-x0));
			}
			x0=pt->x[i];
			region++;
			if(region<rgn)
			{
				curLyr=LayerList[region];
			}

		}
	}
	
	for(int i=0;i<size;i++)//reverse
		photo0[i]*=BtmRF;
	for(int i=pt->PointNumber-1;i>=0;i--)
	{
		if(pt->step[i]==0)// into another region
		{			
			int waveNum=photo0.size();
			for(int k=0;k<waveNum;k++)
			{
				absi=1e-2*Interp(curLyr->absWave,curLyr->alpha,waveLength[k]);
				photo0[k]=photo0[k]*exp(-absi*(x0-pt->x[i]));
			}
			x0=pt->x[i];
			region--;
			if(region>=0)
			{
				curLyr=LayerList[region];
			}
		}
		genSum=0;
		for(int j=0;j<size;j++)//waveLength[j]
		{
			absi=1e-2*Interp(curLyr->absWave,curLyr->alpha,waveLength[j]);
			geni=absi*photo0[j]*exp(-absi*(x0-pt->x[i]));
			genSum+=geni;
		}
		pt->G[i]+=genSum;
		cumAbs += genSum;
		
		//Save off the absorbed photon count if we are asked for it (i.e. to calculate an IQE)
	}
	/*	ofstream file("temp/gen.tmp");
	for(int i=0;i<PointNumber;i++)
	{
		file<<pt->x[i]<<' '<<pt->G[i]<<endl;
	}
	file.close();*/
	
	if (escaped != NULL)
		escaped->push_back(cumAbs);
	
	return 1;
}

void AmpsManager::setSurfaceRecombination(double topSn, double topSp, double bottomSn, double bottomSp)
{
	ambient.topSn = topSn;
	ambient.topSp = topSp;
	ambient.bottomSn = bottomSn;
	ambient.bottomSp = bottomSp;
	
}

void AmpsManager::setReflectance(double front, double back)
{
	ambient.topReflectance = front;
	ambient.bottomReflectance = back;
}
		
void AmpsManager::enableLight(std::vector<double> wavelengths, std::vector<double> photons)
{
	ambient.light = true;
	ambient.wavelengths = wavelengths;
	ambient.photons = photons;
}


void AmpsManager::disableLight()
{
	ambient.light = false;
}

void AmpsManager::setVoltages(std::vector<double> voltages)
{
	ambient.voltages = voltages;
}

void AmpsManager::enableEQE(std::vector<double> waves)
{
	ambient.doQE = true;
	this->wavelengths = waves;
}

void AmpsManager::disableEQE()
{
	ambient.doQE = false;
}

std::vector<double> AmpsManager::getCurrents()
{
	return currents;
}

std::string AmpsManager::version()
{
	return std::string(AMPS_VERSION);
}

void AmpsManager::configureIQECutoff(double cutoff)
{
	this->iqeCutoff = cutoff;
}
