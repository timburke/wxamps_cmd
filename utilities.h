//utilities.h

#ifndef __utilities_h__
#define __utilities_h__

template <class T>
void ClearVector(std::vector<T*> &v)
{
	size_t n = v.size();
	
	for (size_t i=0; i<n; ++i)
		delete v[i];
	
	v.clear();
}
		

#endif