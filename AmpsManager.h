//AmpsManager.h

#ifndef __AMPSMANAGER_H__
#define __AMPSMANAGER_H__

#define AMPS_VERSION "WxAmps (modified) Version 0.5"

#include "string.h"

////@begin includes
////@end includes
#include <fstream>
#include <iomanip>

#include "Point.h"
#include "Point2.h"
//#include "ambient.h"
//#include "resdisplay.h"
//#include "settingdlg.h"
#include "tinyxml.h"
#include <vector>
#include <string>

/*
	wxString m_temp;
    //wxString m_biasV;
    bool m_light,IsWF,IsQE;
	wxString m_specPath;
//	wxString VStart,VEnd,Step1,Step2,Step3,Step4,Switch1,Switch2,Switch3,VolStream;
	wxString TopWF,BtmWF,TopSn,TopSp,TopRF,BtmSn,BtmSp,BtmRF;
	vector<double> waveLength;//m
	vector<double> photonNum;
	vector<double> vSet;
*/

typedef struct
{
	double temperature;
	
	bool light;
	bool useWF;
	bool doQE;
	
	double topWF;
	double bottomWF;

	double topSn;
	double topSp;
	double bottomSn;
	double bottomSp;
	
	double topReflectance;
	double bottomReflectance;
	
	std::vector<double> voltages;
	
	std::vector<double> wavelengths;
	std::vector<double> photons;
} AmbientConditions;

typedef struct
{
	double eps;
	double F0;
	double clamp;
	
	int model;
	int iterTimes;
} Settings;

class AmpsManager 
{
	private:
		Point2		*pt;
		Settings 	settings;
		MyList 		LayerList;
		AmbientConditions ambient;
		
		double iqeCutoff;
		
		std::vector<double> wavelengths;
		std::vector<double> quantumEfficiencies;
		std::vector<double> internalQE;
		
		std::vector<double> currents;
		
		void loadLayer(TiXmlElement *LyrElement, layer* curLyr);
		int calculateThickness();
		bool ExportG(Point *pt,double TopRF,double BtmRF,vector<double> photo0,vector<double> waveLength, vector<double> *absorbed=NULL);
		
	public:
		AmpsManager();
		~AmpsManager();
		
		void loadDevice(const std::string &path);

		//Reverse the loaded device
		void reverse();
		
		void solve();
		void eqe(const std::string &path);
		
		//Accessor Functions
		std::vector<double> getEQEWavelengths();
		std::vector<double> getEQEResults();
		std::vector<double> getIQEResults();
		std::vector<double> getCurrents();
		
		//Setter Functions
		void enableEQE(std::vector<double> waves);
		void disableEQE();
		void setSurfaceRecombination(double topSn, double topSp, double bottomSn, double bottomSp);
		void setReflectance(double front, double back);
		void setVoltages(std::vector<double> voltages);
		
		//Configuration functions
		void configureIQECutoff(double cutoff);
		
		void enableLight(std::vector<double> wavelengths, std::vector<double> photons);
		void disableLight();
		
		std::string version();
};

#endif