//string.cpp

#include "string.h"
#include <stdlib.h>

wxString::wxString(const char *rhs) : str(rhs)
{

}

wxString::wxString()
{

}
	

void wxString::ToDouble(double *out)
{
	double val = strtod(str.c_str(), NULL);
	
	*out = val;
}

void wxString::ToLong(long *out)
{
	long val = atol(str.c_str());
		
	*out = val;
}

const char * wxString::To8BitData() const
{
	return str.c_str();
}

wxString & wxString::operator=(const char *rhs)
{
	str = rhs;
	
	return *this;
}

bool wxString::operator!=(const char *rhs) const
{
	return(str != rhs);
}

wxString wxString::FromAscii(const char *s)
{
	return wxString(s);
}