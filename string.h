#ifndef __STRING_H__
#define __STRING_H__

#define _(x)	(x)

#include <string>

class wxString
{
	private:
	std::string str;
		
	public:
	wxString(const char *rhs);
	wxString();
	
	void ToLong(long *out);
	void ToDouble(double *out);
	
	const char *To8BitData() const;
		
	wxString & operator=(const char *rhs);
	bool operator!=(const char *rhs) const;
	
	static wxString FromAscii(const char *s);
};

#endif