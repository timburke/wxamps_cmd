#!/usr/bin/python

from distutils.core import setup,Extension

setup(	name='wxsolver',
		version='1.0',
		description='Python interface to the wxAmps thin-film solar cell simulation software package.',
		author='Tim Burke',
		author_email='timburke@stanford.edu',
		ext_modules=[Extension('_wxsolver', ['wxsolver.i', 'Calculation.cpp','LU2.cpp','NonEqui.cpp','Point.cpp','Point2.cpp','calexp.cpp','calptu.cpp','normalizedPoint.cpp','tat.cpp','tinystr.cpp','tinyxml.cpp','tinyxmlerror.cpp','tinyxmlparser.cpp','traps.cpp','tunnel.cpp','AmpsManager.cpp','string.cpp'],
					swig_opts=['-c++'],
					)],
		py_modules=['wxsolver'],
	)
	