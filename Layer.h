#ifndef __LAYER_H__
#define __LAYER_H__

#include "string.h"

#include <vector>

struct defect
{
	int type,dist;
	wxString den, el, width, sigN, sigP;
	wxString name;
};

typedef std::vector<defect*> DefectList;

class layer{
public:
	wxString thickness,Eg,Epson,affi,Nc,Nv,un,up,n,p;
	wxString name;

	DefectList defList;
	bool BgShape, IsBT; wxString EgOpt;
	wxString CBTEnergy, CBTGo,CBTSigN,CBTSigP,VBTEnergy, VBTGo,VBTSigN,VBTSigP;
	wxString MGEnergy,GMGA,SigNMGA,SigPMGA,GMGD,SigNMGD,SigPMGD;
	wxString hEdge, hCenter;

	wxString absA,absB;
	std::vector<double> absWave;
	std::vector<double> alpha;
	layer();
	layer(layer& matPar);
	~layer();
	void copy(layer& matPar);
};

typedef std::vector<layer*> MyList;

struct resultPar
{
	double Voc, Jsc, FF, Effi;
	int max;
	std::vector<double> I;
	std::vector<double> V;	
	std::vector<double> QEffi;
};

struct result
{
	double v;
	std::vector<double> x;
	std::vector<double> Jn,Jp,J;
	std::vector<double> R,G;
	std::vector<double> Ec,Ev,Efn,Efp;
	std::vector<double> n,p,Ntd,Nta,nt,pt;
	std::vector<double> EleF;
};

typedef std::vector<result*> ResultArray;

#endif