%module wxsolver

%include stl.i

%template(DoubleVector) std::vector<double>;

%typemap(out) DoubleVector { 
   int i; 
   int len = $1.size();
   $result = PyList_New(len); 
   for (i = 0; i < len; i++) { 
     PyObject *o = PyFloat_FromDouble( $1[i]); 
     PyList_SetItem($result,i,o); 
   } 
}

%{
#include "AmpsManager.h"
%}

%include "AmpsManager.h"