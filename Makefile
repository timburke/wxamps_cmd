CXXFLAGS = -g -msse2 -ffast-math -ftree-vectorize -march=core2 -ftree-vectorizer-verbose=2
LDFLAGS = -g -mmacosx-version-min=10.7 -march=core2

OFILES = Calculation.o \
		 LU2.o \
		 NonEqui.o \
		 Point.o \
		 Point2.o \
		 calexp.o \
		 calptu.o \
		 normalizedPoint.o \
		 tat.o \
		 tinystr.o \
		 tinyxml.o \
		 tinyxmlerror.o \
		 tinyxmlparser.o \
		 traps.o \
		 tunnel.o \
		 AmpsManager.o\
		 string.o\

all: cmdline

swig: $(OFILES)
	swig -c++ -python -o wxsolver_wrap.cpp wxsolver.i
	g++ -c `python-config --cflags` $(CXXFLAGS) wxsolver_wrap.cpp -o wxsolver_wrap.o
	g++ -bundle `python-config --ldflags` $(LDFLAGS) $(OFILES) wxsolver_wrap.o -o _wxsolver.so
	

cmdline: $(OFILES) cmd_main.o
	g++ $(LDFLAGS) $(OFILES) cmd_main.o -o cmd_wxamps

%.o : %.cpp
	g++ $(CXXFLAGS) -c $< -o $@

clean:
	@rm $(OFILES) cmd_main.o