//#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

////@begin includes
////@end includes
#include <fstream>
#include <iomanip>
//#include "mainamps.h"
//#include "ambient.h"
//#include "materialdlg.h"
//#include "resdisplay.h"

#include "Calculation.h"
#include "NonEqui.h"
#include "Constant.h"
#include "Point.h"
#include "Point2.h"
#include "LU.h"
//#include "main.rc"
using namespace std;

#include "AmpsManager.h"

int main(int argc, char **argv)
{
	std::string path("/Users/timburke/Documents/Graduate School/Research/Upconversion/Scripts/data/devices/wxamps/CdTe_Gloeckler.dev");
	
	AmpsManager manager;
	
	vector<double> waves;
	vector<double> photons;
	vector<double> voltages;
	
	voltages.push_back(0.0);
	voltages.push_back(0.5);
	
	waves.push_back(500);
	waves.push_back(600);
	
	photons.push_back(1e15);
	photons.push_back(1e16);
	
	manager.enableEQE(waves);
	//manager.setVoltages(voltages);
	//manager.enableLight(waves, photons);
	
	manager.loadDevice(path);
	
	//for (int i=0; i<40; ++i)
		manager.solve();
	
	vector<double> results = manager.getEQEResults();
	
	for(int i=0; i<results.size(); ++i)
	{
		printf("Wavelength %d: %g\n", (int)waves[i], results[i]);
	}
	
	return 0;
}